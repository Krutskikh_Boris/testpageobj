import locators_all.Locators;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

public class Senex extends Locators {
    /**
     * Нажимаем на кнопку войти
     * Имя селектора вытягиваем из класса Locators (logInPopUpBtn)
     * <p>
     * $ - найти
     */
    public void clickEnterBtn() {
        $(LOG_IN_POP_UP).click();
    }

    /**
     * Заполняем поля валидными данными
     * Для ввода данных создаем сразу Строковые переменные что бы использовать их в main классе AutoTest
     * То есть нам нужно создать String name, String email, String password
     * Каждое значение передаем своему локатору
     */
    public void setFieldsValidDataAndRegister(String name, String email, String password) {
        $(NAME_FIELD).setValue(name);
        $(EMAIL_FIELD).setValue(email);
        $(PASSWORD_FIELD).setValue(password);

        //Отмечаем чекер
        $(AGREE_CHECKBOX).click();

        //Кликаем по кнопке "Зарегистрироваться"
        //$(regBtn).click();

        //test
        //test2
        //test3
        //test4


    }

    /**
     * Вызываем 2 вышеуказанных метода
     * В метод initializeTest так же нужно передать String параметры
     * <p>
     * Теперь метод initializeTest мы будем вызывать в main классе AutoTest
     * <p>
     * методы вызванные в initializeTest будут выполняться поочереди (сверху вниз)
     */
    public void initializeTest(String name, String email, String password) {
        this.clickEnterBtn();
        this.setFieldsValidDataAndRegister(name, email, password);
    }
}
