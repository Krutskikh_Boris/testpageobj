package locators_all;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

/*
    * Локаторы видов:
    * CSS
    * Xpath
    * ID
    * linkText
    * tagName
    * className
    * */

/**
 * В этот класс мы будем выносить все локаторы элементов необходмых для работы
 */
public class Locators {

    //Кнопка войти
    protected SelenideElement LOG_IN_POP_UP = $(byXpath("//*[@id=\"mm-0\"]/div[2]/header/hgroup[1]/div/div/div[2]/nav/div[4]/button/div/span/span"));

    //Поля (Имя, Пароль, Емейл)
    protected SelenideElement NAME_FIELD = $(byId("name"));
    protected SelenideElement EMAIL_FIELD = $(byId("email2"));
    protected SelenideElement PASSWORD_FIELD = $(byId("password1"));

    //Чекер о принятии условии пользовательского соглашения
    protected SelenideElement AGREE_CHECKBOX = $(byXpath("/html/body/div[2]/div/div[1]/div/div/div[2]/div[2]/div[4]/label/ins"));

    //Кнопка зарегистрироваться
    protected SelenideElement REG_BTN = $(byXpath("/html/body/div[2]/div/div[1]/div/div/div[2]/div[2]/div[5]/button"));
}
