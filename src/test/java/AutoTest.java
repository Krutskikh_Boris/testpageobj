import com.codeborne.selenide.Configuration;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;

public class AutoTest {

    //Создадим объект класса Senex
    Senex regNewUser;

    //Создадим переменную для ссылки
    final String BASE_URL = "http://senex.com.ua";

    /**
     * В аннотации @BeforeTest добавим необходимые конфигурации перед запуском
     */
    @BeforeTest
    public void startPage() {

        //Инициализация хром драйвера
        Configuration.browser = "chrome";

        //путь к драйверу
        System.setProperty("webdriver.chrome.driver", "C:/chromedriver.exe");
    }

    /**
     * В аннотации @Test мы будем вызывать наш Senex класс
     * <p>
     * regNewUser = new Senex(); экземпляр класса
     * regNewUser.initializeTest - вызов метода initializeTest с класса Senex со String параметрами
     * Тоисть уже в этих String переменных мы вводим нужные нам данные
     */
    @Test
    public void regUserTest() {
        open(BASE_URL);
        regNewUser = new Senex();
        regNewUser.initializeTest(
                "TestUser",
                "test@test.com",
                "qwerty");
    }
}
